# Attribution

Not all of the work in this repository is mine. This document will denote
the source of anything that was not developed by me, but tweaked for my
purposes. These files are licensed under their respective licenses. Please
refer to the documentation at the links below for the parts listed.

1. The stl/X_End_Idler.stl and stl/X_End_Motor.stl files are revised
   versions of the Wilson II scad/x-end.scad. The Wilson II was
   designed by user mjrice and it is a beauty. The adjustments made
   allow it to use a LM8UU bearing and 8mm steel rods. This was taken
   from the original Wilson II GitHub repository and can be found at
   <https://github.com/mjrice/Wilson2/blob/master/scad/x-end.scad>. Files
   for the Wilson II were also available on Thingiverse at
   <https://www.thingiverse.com/thing:1086101>, but now this link references
   the GitHub repository instead. It is licensed under the Creative Commons -
   Attribution license.
2. The stl/Compact_Bowden_Extruder.stl and stl/Compact_Bowden_Idler.stl
   files were generated from the scad/compact_direct_drive_extruder.scad
   file by Thingiverse user schlotzz with minor modifications. The
   modifications lengthen the frame attachment bracket, resize the holes
   on the bracket to support M5 screws, make the base thick enough to use
   10 mm screws to attach the extruder to the motor, and adjust things to
   use an MK7 drive gear. This file is licensed under the Creative
   Commons - Attribution - Share Alike license. It can be downloaded at
   <https://www.thingiverse.com/thing:275593>.
3. I also drew inspiration for the adjustments necessary for an
   MK7 gear from the Compact Bowden Extruder, 1.75mm Filament for
   MK7 Drive Gear by Thingiverse user Tech2C. It can be found at
   <https://www.thingiverse.com/thing:767951>. It was derived from
   scholtzz's design and is licensed under the Creative Commons -
   Attribution - Non-Commercial license.
4. I cannot claim the scad/Belt_Guide_608.scad was entirely my work. I found a
   design on Thingiverse, which I can't seem to find again. It wasn't for a 608
   bearing, so I modified it and made it work for what I wanted. The original
   design included outer rings to keep the belt from sliding around. The Wilson
   II uses a Y-shaped guide which should keep the belt on the idler, so I did
   away with the rims and chose to go with a small central guide that should keep
   the 608ZZ bearing centered and the belt running smooth. This design has a
   flaw though, since the original X and Y idlers were designed for a smaller
   bearing, they now need a redesign for the larger bearing. Look for that in
   the near future.
5. The stl/Ramps_Mount_70mm.stl file is a fairly precise OpenScad copy of the
   Wilson II original ramps-mount.stl file with modifications to allow it to
   fit a 70mm fan and to include ventilation under the Arduino Mega. The
   original file can be found on GitHub at
   <https://github.com/mjrice/Wilson2/blob/master/stl/ramps-mount.stl>. The
   OpenScad source file used to create the modified version is
   scad/Ramps_Mount_70mm.scad file.

Finally, the Creative Commons Attribution Markdown license was pulled from
<https://github.com/idleberg/Creative-Commons-Markdown>.
