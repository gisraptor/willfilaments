use <vitamins.scad>;

//translate([933.16, 133.85, -5.45]) import("ramps-mount.stl");

$fn = 64;
side_ln = 7.68*tan(30);
width_across_corners = pow(pow(7.68, 2) + pow(side_ln, 2), 0.5);
fan_size = 70;
base_fan_size = 80;
fan_size_diff = base_fan_size - fan_size;
half_diff = fan_size_diff/2;

color("green")
difference() {
    union() {
        // Base
        translate([43.16,0,0]) cube([18.1,132,3.5]);
        translate([0,5.49,0]) cube([68, 123.41, 3.5]);
        // Board mount points
        translate([13.05,31.2,0]) cylinder(r=3.52, h=7.504);
        translate([61.23,29.9,0]) cylinder(r=3.52, h=7.504);
        translate([28.25,82.01,0]) cylinder(r=3.52, h=7.504);
        translate([56.15,82.02,0]) cylinder(r=3.52, h=7.504);
        translate([13.05,106.1,0]) cylinder(r=3.52, h=7.504);
        translate([61.255,112.425,0]) cylinder(r=3.52, h=7.504);
        // Fan mount base wall
        translate([0,5.49,0]) cube([6.9717,123.41,8.342]);
        // Bracket mount grooves
        translate([52.21,6.1,0]) cylinder(r=5.72, h=4.205);
        translate([52.21-5.72,0,0]) cube([5.72*2,6,4.205]);
        translate([52.21,125.2,0]) cylinder(r=5.72, h=4.205);
        translate([52.21-5.72,125.05,0]) cube([5.72*2,6.9,4.205]);
        // Fan mount
        translate([0, 31.38, 0]) cube([3.97, 80-fan_size_diff, 29.43]);
        translate([0, 31.38, 0]) cube([6.97, 80-fan_size_diff, 13.183]);
        translate([3.97, 31.55, 8.342]) cube([1, 7.68, 10.43]);
        hexagon(width=7.68, depth=1, sides=6, rotation=[0, 90, 0],
                translation=[3.97, 31.55+(7.68/2), 14+(width_across_corners/2)]);
        translate([3.97, 103.06-fan_size_diff, 8.342]) cube([1, 7.67, 10.43]);
        hexagon(width=7.68, depth=1, sides=6, rotation=[0, 90, 0],
                translation=[3.97, 103.06-fan_size_diff+(7.68/2), 14+(width_across_corners/2)]);
        intersection() {
            translate([0, 40, 0]) cube([6.97, 61.5-fan_size_diff, 19.181]);
            translate([-17, 40+((61.8-fan_size_diff)/2), 13])
            sphere(r=((70.25-half_diff)/2));
        }
    }
    union() {
        // Base
        translate([23.7,16,1.01]) cube([27.1,30,10]);
        translate([13.35,41,1.01]) cube([47.6,30,10]);
        translate([23.7,86,1.01]) cube([27.1,30,10]);
        translate([7,31.2-3.52,-0.1]) cube([1.5, 7.04, 10]);
        translate([7,106.1-3.52,-0.1]) cube([1.5, 7.04, 10]);
        // Board mount points
        translate([13.05,31.2,-0.1]) cylinder(r=1.98, h=10);
        translate([61.23,29.9,-0.1]) cylinder(r=1.98, h=10);
        translate([28.25,82.01,-0.1]) cylinder(r=1.98, h=10);
        translate([56.15,82.02,-0.1]) cylinder(r=1.98, h=10);
        translate([13.05,106.1,-0.1]) cylinder(r=1.98, h=10);
        translate([61.255,112.425,-0.1]) cylinder(r=1.98, h=10);
        // Bracket mount grooves
        translate([52.21,6.1,-0.1]) cylinder(r=2.72, h=7);
        translate([52.21-2.72,-0.1,-0.1]) cube([2.7*2,6,7]);
        translate([52.21,125.2,-0.1]) cylinder(r=2.72, h=7);
        translate([52.21-2.72,125.15,-0.1]) cube([2.72*2,6.9,7]);
        // Fan mount
        translate([-1,71.38-half_diff,59]) rotate([0,90,0]) cylinder(r=40, h=20);
        translate([-1, 31.55+(7.68/2), 14+(width_across_corners/2)])
        rotate([0,90,0]) cylinder(r=2.25, h=20);
        translate([-1, 103.06-fan_size_diff+(7.68/2), 14+(width_across_corners/2)])
        rotate([0,90,0]) cylinder(r=2.25, h=20);
        hexagon(width=6.32, depth=4, sides=6, rotation=[0, 90, 0],
                translation=[3.473, 31.55+(7.68/2), 14+(width_across_corners/2)]);
        hexagon(width=6.32, depth=4, sides=6, rotation=[0, 90, 0],
                translation=[3.473, 103.06-fan_size_diff+(7.68/2), 14+(width_across_corners/2)]);
        // Base Vents
        intersection() {
            union() {
                translate([23.7,16,-10]) cube([27.1,30,20]);
                translate([13.35,41,-10]) cube([47.6,30,20]);
                translate([23.7,86,-10]) cube([27.1,30,20]);
            }
            union() {
                translate([36,2,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([41,2,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([46,2,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([51,2,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([56,2,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([61,2,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([66,2,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([71,2,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([76,2,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([81,2,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([86,2,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([91,2,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([96,2,-1]) rotate([0,0,30]) cube([2, 85, 20]);

                translate([26,82,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([31,82,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([36,82,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([41,82,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([46,82,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([51,82,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([56,82,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([61,82,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([66,82,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([71,82,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([76,82,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([81,82,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([86,82,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([91,82,-1]) rotate([0,0,30]) cube([2, 85, 20]);
                translate([96,82,-1]) rotate([0,0,30]) cube([2, 85, 20]);
            }
        }
    }
}
