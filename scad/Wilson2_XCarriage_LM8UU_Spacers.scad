$fn=64;

translate([100,0,0]) %import("x-carriage.stl", convexity=20);    

translate([100,25,0])
union() {
    difference() {
        union() {
            translate([22.6, 7.95, (59-4.2)/2])
            difference() {
                cylinder(h=4.2, r=18.8/2, center=false);
                translate([0,0,-0.4])
                cylinder(h=5, r=13/2, center=false);
            }
            translate([22.6, 7.95, 0])
            difference() {
                // Fill existing hole
                cylinder(h=59, r=18.8/2, center=false);
                // Remove center for bearings
                translate([0, 0, -0.5])
                cylinder(h=60, r=15.5/2, center=false);
            }
        }
        union () {
            translate([15.88, -4, -0.5])
            rotate([0,0,-25]) cube([6,12, 60]);
            translate([23.650, -6, -0.5])
            rotate([0,0,25]) cube([5,12, 60]);
        }
    }
    difference() {
        union() {
            translate([-27.39, 7.95, (59-4.2)/2])
            difference() {
                cylinder(h=4.2, r=18.8/2, center=false);
                translate([0,0,-0.4])
                cylinder(h=5, r=13/2, center=false);
            }
            translate([-27.39, 7.95, 0])
            difference() {
                // Fill existing hole
                cylinder(h=59, r=18.8/2, center=false);
                // Remove center for bearings
                translate([0, 0, -0.5])
                cylinder(h=60, r=15.5/2, center=false);
            }
        }
        union () {
            translate([-34.12, -4, -0.5])
            rotate([0,0,-25]) cube([6,12, 60]);
            translate([-26.365, -6, -0.5])
            rotate([0,0,25]) cube([5,12, 60]);
        }    
    }
}