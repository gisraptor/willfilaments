//
// Parametric belt guide for 608 bearings and their ilk.
// 

layer_height = 0.3;
hotend_width = 0.4;

wall= hotend_width*3;
clearance= hotend_width/2 - 0.05; 

// LM8UU
lm8_bearing_id = 8;
lm8_bearing_od = 15;
lm8_bearing_ln = 24;

// LM10UU
lm10_bearing_id = 10;
lm10_bearing_od = 19;
lm10_bearing_ln = 29;
lm10_print_bearing_ln = round(lm10_bearing_ln/layer_height)*layer_height;

module bearing_spacer_body() {
    cylinder(
        r=lm10_bearing_od/2,
        h=lm10_print_bearing_ln,
        $fn=50
    );
}

module zip_tie_mount() {
    gap = 16;
    total_length = 30;
    zip_tie_hole = 3.5;
    difference() {
        translate([
            -2,
            (lm8_bearing_od/2)+0.5,
            2])
        cube([
            4,
            4,
            lm10_print_bearing_ln-2]);
        union() {
            rotate_extrude(
                angle=45,
                convexity=10,
                $fn=50) {
                translate([
                    9.52,
                    (total_length-gap-(zip_tie_hole*2))/2,
                    0])
                square([
                    1.5,
                    zip_tie_hole],
                    center=false
                );
            }
            rotate_extrude(
                angle=45,
                convexity=10,
                $fn=50) {
                translate([
                    9.52,
                    ((total_length-gap-(zip_tie_hole*2))/2)+gap,
                    0])
                square([
                    1.5,
                    zip_tie_hole],
                    center=false
                );
            }
        }
    }   
}

module center_scaled_cylinder(length, radius, scale_percentage) {
    //translate([0,0,lm10_print_bearing_ln-lm8_bearing_ln-0.2])
    linear_extrude(
        height=(length/2)+0.5,
        center=false,
        convexity=10,
        scale=scale_percentage,
        $fn=50
    )
    circle(r=radius, $fn=50);

    //color("red")
    translate([0,0,length+0.25])
    rotate([0,180,0]) {
        linear_extrude(
            height=(length/2)+0.5,
            center=false,
            convexity=10,
            scale=scale_percentage,
            $fn=50
        )   
        circle(r=radius, $fn=50);
    }    
}

module bearing_spacer_holes() {
    union() {
        translate([0, 0, lm10_print_bearing_ln-lm8_bearing_ln])
//        cylinder(
//            r=(lm8_bearing_od)/2,
//            h=lm8_bearing_ln + 0.5,
//            $fn=50
//        );
        center_scaled_cylinder(
            length=lm8_bearing_ln,
            radius=lm8_bearing_od/2,
            scale_percentage=[0.95,0.95]
        );        
        translate([0, 0, lm10_print_bearing_ln-(7*layer_height)])
        cylinder(
            r1=(lm8_bearing_od+hotend_width)/2,
            r2=(lm8_bearing_od+(hotend_width*7))/2,
            h=8*layer_height,
            center=1
        );
        rotate_extrude(angle=360, convexity=10, $fn=50) {
            //rotate([90,0,0])
            polygon( points=[
                [(lm10_bearing_od/2)+0.1,0],
                [(lm10_bearing_od/2)+0.1,2],
                [((lm10_bearing_od/2)-0.75),0],
            ] );
        }
        translate([0,0,-1])
        cylinder(
            r1=5.25,
            r2=4.25,
            h=lm10_print_bearing_ln-lm8_bearing_ln+2,
            $fn=50
        );
    }   
}

module bearing_spacer_holding_rings() {
    union() {
        rotate_extrude(angle=360, convexity=10) {
            translate([
                -lm8_bearing_od/2,
                lm10_print_bearing_ln - lm8_bearing_ln + 4.5,
                0])
            circle(
                r=0.3,
                $fn=50
            );
        }
        rotate_extrude(angle=360, convexity=10) {
            translate([
                -lm8_bearing_od/2,
                lm10_print_bearing_ln - 4.5,
                0])
            circle(
                r=0.3,
                $fn=50
            );
        }
    }
}

module bearing_spacer() {
    union() {
        difference() {
            bearing_spacer_body();
            #bearing_spacer_holes();
        }
        bearing_spacer_holding_rings();
    }
}

module bearing_spacer_for_y_mount() {
    union() {
        difference() {
            union() {
                bearing_spacer_body();
                zip_tie_mount();
            }
            #bearing_spacer_holes();
        }
        bearing_spacer_holding_rings();
    }    
}

bearing_spacer();
//translate([22, 0, 0]) bearing_spacer_for_y_mount();

