//
// Parametric belt guide for 608 bearings and their ilk.
// 

layer_height = 0.3;
hotend_width = 0.4;

wall= hotend_width*3;
clearance= hotend_width/2 - 0.05; 

// for 608 bearings:
bearing_d = 22;
bearing_h = 7;
bearing_inside_d = 8;

guide_outer_d = 9.5;
guide_post_outer_d = 8;
guide_inner_d = 4.2;
guide_outer_h = 8.7; // total height wanted for the assembled belt guide
guide_post_h = round((guide_outer_h/2)/layer_height)*layer_height;
echo(guide_post_h);
guide_flange_h = round(((guide_outer_h-bearing_h)/2)/layer_height)*layer_height;
echo(guide_flange_h);

module bearing() {
    translate([0,0,guide_flange_h+0.1])
    difference() {
        cylinder(r=bearing_d/2, h=bearing_h, $fn=50);
        translate([0,0,-1])
        // bearing inner hole
        cylinder(r=bearing_inside_d/2, h=bearing_h + 2, $fn=50);
    };
}

module belt_guide_base() {
 cylinder(r=guide_post_outer_d/2-hotend_width, h=guide_post_h, $fn=50);
 // outer flange
 cylinder(r=guide_outer_d/2+(hotend_width*4), h=guide_flange_h, $fn=50);
}

module belt_guide_holes(){
 // hole for the bearing:
 //#translate([0,0,guide_outer_h/2 - bearing_h/2 + layer_height]) cylinder(r=bearing_d/2+clearance, h=bearing_h, $fn=50);
 #bearing();
 // hole for the rod through:
 translate([0,0,-1]) cylinder(r=bearing_inside_d/2-wall, h=10, $fn=50);
}

// Final part
module belt_guide(){
 difference(){
  belt_guide_base();
  belt_guide_holes();
 }
}

belt_guide();


