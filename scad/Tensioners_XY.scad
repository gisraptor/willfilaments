// X and Y Tensioners for the Wilson T2

$fn=64;

use <vitamins.scad>;

x_width = 9;
y_width = 10;
y_inner_bearing_width = 10;
y_outer_bearing_width = 15;
y_height = 12;
y_body_ln = 37.5;
y_bearing_ln = 25;
letter_height = 1;

module mirror_copy(mirror_vector=[1,0,0]) {
    children();
    mirror(mirror_vector) { children(); }
}

module y_tensioner(
    width,
    inner_spacing_width,
    outer_spacing_width,
    height,
    body_ln,
    bearing_ln,
    marker_height,
	rotation=[0,0,0],
	translation=[0,0,0]) {


    translate(translation)
    rotate(rotation) {
        tensioner_arm_width = (outer_spacing_width - inner_spacing_width)/2;
        x_offset = marker_height+tensioner_arm_width;
        cyl_radius = height/2+2;
        tensioner_x_offset = width/2;
        tensioner_z_offset = height/2;
        tensioner_radius = 7/2;
        translate([x_offset, 0, 0])
        union () {
            translate([-4.4,0,6.06]) rotate([90,180,90]) color("DarkRed") linear_extrude(height=1) text("Y", font="DejaVu Sans:style=Condensed Bold", size=6);
            difference() {
                cube([width, body_ln, height]);
                translate([tensioner_x_offset,5.7,tensioner_z_offset])
                rotate([-90,0,0])
                color("purple")
                difference() {
                    cylinder(r=cyl_radius, h=body_ln-4);
                    translate([0,0,-1]) cylinder(r=cyl_radius-1, h=body_ln+4);
                }
                translate([-0.5,-11,height/2]) rotate([0,90,0]) cylinder(r=13, h=width+1);
                translate([-1,body_ln-13-14,(height-4)/2]) cube([width+2,14,4]);
                translate([(width/2),body_ln+1,(height/2)]) rotate([90,0,0]) color("green") cylinder(r=1.5, h=body_ln-13);
                translate([(width/2),body_ln+1,(height/2)]) rotate([90,0,0]) color("green") cylinder(r=2, h=6);
                translate([(width/2),body_ln-4.5,(height/2)]) color("blue") hexagon(width=5.5, depth=2.4,side=6,rotation=[90,0,0]);
                translate([width-(width/2),body_ln-6.9,(height-5.5)/2]) color("yellow") cube([(width/2)+1,2.4,5.5]);
            }
            translate([0,2.21,0])
            difference() {
                union() {
                    cylinder(r=tensioner_radius, h=height);
                    translate([width, 0, 0]) cylinder(r=tensioner_radius, h=height);
                }
                translate([-tensioner_radius-1, -tensioner_radius-0.5, -1])
                cube([width+(2*tensioner_radius)+2, tensioner_radius+1, height+2]);
            }
            difference() {
                union() {
                    translate([-tensioner_radius+0.05,-15.2,0]) cube([tensioner_radius,18,height]);
                    translate([inner_spacing_width-0.046,-15.2,0]) cube([tensioner_radius,18,height]);
                }
                translate([-5,-11,height/2]) rotate([0,90,0]) cylinder(r=2, h=outer_spacing_width+5);
            }
            difference() {
                translate([-tensioner_radius+0.05,-3.675,height/2]) rotate([0,90,0]) cylinder(r=13, h=tensioner_radius);
                translate([-tensioner_radius-0.45,-15.2,-10]) cube([tensioner_radius+2,25,90]);
            }
            difference() {
                translate([inner_spacing_width-0.046,-3.675,height/2]) rotate([0,90,0]) cylinder(r=13, h=tensioner_radius);
                translate([inner_spacing_width-0.446,-15.2,-10]) cube([tensioner_radius+2,25,90]);
            }
        }
    }
}


module x_tensioner(
    width,
    inner_spacing_width,
    outer_spacing_width,
    height,
    body_ln,
    bearing_ln,
    marker_height,
	rotation=[0,0,0],
	translation=[0,0,0]) {


    translate(translation)
    rotate(rotation) {
        tensioner_arm_width = (outer_spacing_width - inner_spacing_width)/2;
        x_offset = marker_height+tensioner_arm_width;
        cyl_radius = height/2+2;
        tensioner_x_offset = width/2;
        tensioner_z_offset = height/2;
        tensioner_radius = 7/2;
        tensioner_arm_support_width = width+(2*tensioner_radius)-2.5;
        translate([x_offset, 0, 0])
        union () {
            translate([(width-5.7)/2,10,height-0.1]) color("DarkGreen") linear_extrude(height=1) text("X", font="DejaVu Sans:style=Condensed Bold", size=6);
            difference() {
                union() {
                    translate([0,3,0]) cube([width, body_ln-3, height]);
                    translate([-(tensioner_arm_support_width-width)/2,2.5,0]) cube([tensioner_arm_support_width, tensioner_radius, height]);
                }
                translate([tensioner_x_offset,6.71,tensioner_z_offset])
                rotate([-90,0,0])
                color("purple")
                difference() {
                    cylinder(r=cyl_radius, h=body_ln-4);
                    translate([0,0,-1]) cylinder(r=cyl_radius-1, h=body_ln+4);
                }
                translate([-1,-11,(height+2)/2]) rotate([0,90,0]) cylinder(r=13, h=width+2);
                translate([-1,body_ln-13-14,(height-4)/2]) cube([width+2,14,4]);
                translate([(width/2),body_ln+1,(height/2)]) rotate([90,0,0]) color("green") cylinder(r=1.5, h=body_ln-13);
                translate([(width/2),body_ln+1,(height/2)]) rotate([90,0,0]) color("green") cylinder(r=2, h=6);
                translate([(width/2),body_ln-4.5,(height/2)]) color("blue") hexagon(width=5.5, depth=2.4,side=6,rotation=[90,0,0]);
                translate([width-(width/2),body_ln-6.9,(height-5.5)/2]) color("yellow") cube([(width/2)+1,2.4,5.5]);
            }
            translate([0,2.21,0])
            union() {
                difference() {
                    union() {
                        translate([0,0,0]) cylinder(r=tensioner_radius+1, h=height);
                        translate([inner_spacing_width-1, 0, 0]) cylinder(r=tensioner_radius+1, h=height);
                    }
                    translate([-tensioner_radius-1, -tensioner_radius-3.5, -1])
                    cube([width+(2*tensioner_radius)+4, tensioner_radius+4, height+2]);
                }
                // translate([-tensioner_radius-1, -tensioner_radius+0.7, -1])
                // #cube([width+(2*tensioner_radius)+2, tensioner_radius, height+2]);
            }
            difference() {
                union() {
                    translate([-tensioner_radius-1,-15.2,0]) cube([tensioner_radius,18,height]);
                    translate([inner_spacing_width-0.046,-15.2,0]) cube([tensioner_radius,18,height]);
                }
                translate([-5,-11,8]) rotate([0,90,0]) cylinder(r=2, h=outer_spacing_width+5);
            }
            difference() {
                translate([-tensioner_radius-1,-11,8]) rotate([0,90,0]) cylinder(r=9, h=tensioner_radius);
                translate([-tensioner_radius-1.01,-15.2,-10]) cube([tensioner_radius+2,25,21]);
            }
            difference() {
                translate([inner_spacing_width-0.046,-11,8]) rotate([0,90,0]) cylinder(r=9, h=tensioner_radius);
                translate([inner_spacing_width-0.446,-15.2,-10]) cube([tensioner_radius+2,25,21]);
            }
        }
    }
}

// Show originals for reference
translate([0, 70, 0])
#import("x-tensioner.stl");
translate([-1,25.3,0]) color("orange") cube([1,1.5,10]);
translate([-2,24.7,0]) color("green") cube([1,1.5,6]);

translate([0, 80, 0])
#import("y-tensioner.stl");

x_tensioner(
    x_width,
    y_inner_bearing_width,
    y_outer_bearing_width,
    y_height,
    y_body_ln,
    y_bearing_ln,
    letter_height,
    [0,0,0],
    [20, 32.43, 0]);


y_tensioner(
    y_width,
    y_inner_bearing_width,
    y_outer_bearing_width,
    y_height,
    y_body_ln,
    y_bearing_ln,
    letter_height,
    [0,0,0],
    [20, 91.43, 0]);

