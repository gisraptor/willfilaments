// X carriage for the Wilson T2

$fn=64;

use <vitamins.scad>;
rotate([0,0,90]) translate([200, -52.5, 0]) import("x-carriage.stl");

// printer specifications
hotend_size = 0.4;
layer_height = 0.3;

function printer_horizontal_size(h_size) = round(h_size/hotend_size)*hotend_size;
function printer_vertical_size(v_size) = round(v_size/layer_height)*layer_height;
function unsmoothed_value(v, s) = v-(s*2);
function smoothed_value(v, s) = v+(s*2);
module mirror_copy(mirror_vector=[1,0,0]) {
    children();
    mirror(mirror_vector) { children(); }
}
module tapered_cylinder(r1, r2, h) {
    half_length = h/2;
    //cylinder(r1=r1, r2=r2, h=half_length);
    union() mirror_copy([0,0,1]) cylinder(r1=r1, r2=r2, h=half_length);
}

model_bearing_ln = 29.5;
model_bearing_od = 15;
model_bearing_id = 8;
model_smoothing = 1;
model_rotation = [0,0,0];
model_translation = [0,0,0];

module x_carriage(outer_diameter, inner_diameter, bearing_length, smoothing, rotation, translation) {

outer_radius = (outer_diameter/2);
echo("OR: ", outer_radius);
inner_radius = (inner_diameter/2);
echo("IR: ", inner_radius);
wall = 6;
echo("Wall: ", wall);
width = unsmoothed_value((bearing_length+0.5)*2, smoothing);
echo("Width: ", width);
echo("Full Width: ", smoothed_value(width, smoothing));

    module bearing_case(rotation, translation) {
        outer_walled_radius = unsmoothed_value(outer_diameter+wall, smoothing)/2;
        echo("OWR: ", outer_walled_radius);

        translate(translation) {
            rotate(rotation)
            translate([outer_walled_radius+smoothing,outer_walled_radius+smoothing,-width-smoothing]) {
                union() {
                    difference() {
                        minkowski() {
                            cylinder(r=outer_walled_radius-0.3, h=width);
                            sphere(r=smoothing);
                        }
                        translate([0,0,-3]) {
                            union () {
                                cylinder(r=outer_radius, h=smoothed_value(width, smoothing)+4);
                                rotate([0,0,-65])
                                cube([wall+0.5,outer_walled_radius+2,smoothed_value(width, smoothing)+4]);
                                rotate([0,0,-25])
                                cube([outer_walled_radius+2,wall+0.5,smoothed_value(width, smoothing)+4]);
                            }   
                        }
                    }
                    translate([0,0,(smoothed_value(width, smoothing)-1)/2]) {
                        difference() {
                            cylinder(r=outer_walled_radius, h=1);
                            translate([0,0,-1])
                            cylinder(r=outer_radius-1, h=3);
                            translate([0,-10,-2])
                            cube([outer_diameter,20,5]);
                        }
                    }
                }
            }
        }
    }

    bearing_case([90,0,0], [0,0,0]);
    bearing_case([90,0,0], [0,0,50]);
}

translate([0, 100, 0]) rotate([90, 0, 0]) x_carriage(model_bearing_od, model_bearing_id, model_bearing_ln, model_smoothing, model_rotation, model_translation);


//bearing(translation=[lm8uu_bearing_walled_r,2+width_spacer,lm8uu_bearing_lower_offset]);
//bearing(translation=[lm8uu_bearing_walled_r,width-lm8uu_bearing_ln-(2+width_spacer),lm8uu_bearing_lower_offset]);
//bearing(translation=[lm8uu_bearing_walled_r,2+width_spacer,lm8uu_bearing_lower_offset+rod_distance]);
//bearing(translation=[lm8uu_bearing_walled_r,width-lm8uu_bearing_ln-(2+width_spacer),lm8uu_bearing_lower_offset+rod_distance]);
//rod(translation=[lm8uu_bearing_walled_r,-250,lm8uu_bearing_lower_offset]);
//rod(translation=[lm8uu_bearing_walled_r,-250,lm8uu_bearing_lower_offset+rod_distance]);

// bump_smooth_radius = 1;
//
// lm8uu_bearing_ln = 24;
// lm8uu_bearing_od = 15;
// lm8uu_bearing_id = 8;
// lm8uu_bearing_or = lm8uu_bearing_od/2.0;
// lm8uu_bearing_ir = lm8uu_bearing_id/2.0;
// lm8uu_bearing_wall = 8-(bump_smooth_radius*2);
// lm8uu_bearing_walled_r = (lm8uu_bearing_or/2) + lm8uu_bearing_wall;
// lm8uu_bearing_cut = 2;
//
// rod_distance = 70;
// rod_d = 8;
// rod_r = rod_d/2;
// height = rod_distance + 25;
// additional_width = 4.6;
// width_spacer = additional_width/2;
// width = ((lm8uu_bearing_ln+0.2)*2)+6+additional_width;
// depth = 19.5;
// bearing_depth = 2.85;
//
// lm8uu_bearing_lower_offset = rod_r+8;
// lm8uu_bearing_upper_offset = rod_r+8+rod_distance;
//
// module x_mount_hole(shift) {
//     // Mount holes
//     translate(shift)
//     rotate([0,90,0])
//     cylinder(r=2.1, h=depth+2);
// }
//
// module x_mount_holes() {
//     y_mount_offset_distance = (width-23)/2;
//     z_mount_offset_distance = ((height)-23)/2;
//     for (shift=[[-1,y_mount_offset_distance,z_mount_offset_distance],
//         [-1, y_mount_offset_distance+23,z_mount_offset_distance],
//         [-1, y_mount_offset_distance,z_mount_offset_distance+23],
//         [-1, y_mount_offset_distance+23,z_mount_offset_distance+23] ]) {
//         x_mount_hole(shift);
//     }
// }
//
// module x_carriage_holes() {
//     union() {
//         // Bottom bearing hole
//         translate([lm8uu_bearing_walled_r,-1,lm8uu_bearing_lower_offset])
//         rotate([-90,0,0])
//         cylinder(r=lm8uu_bearing_or+0.1, h=width+2);
//
//         // Top bearing hole
//         translate([lm8uu_bearing_walled_r,-1,lm8uu_bearing_upper_offset])
//         rotate([-90,0,0])
//         cylinder(r=lm8uu_bearing_or+0.1, h=width+2);
//
//         // Bottom left bearing hole taper
//         translate([lm8uu_bearing_walled_r,-0.05,lm8uu_bearing_lower_offset])
//         rotate([-90,0,0])
//         cylinder(r1=lm8uu_bearing_or+1, r2=lm8uu_bearing_or+0.1, h=2.1+width_spacer);
//
//         // Bottom right bearing hole taper
//         translate([0,width,0])
//         mirror([0,1,0])
//         translate([lm8uu_bearing_walled_r,-0.05,lm8uu_bearing_lower_offset])
//         rotate([-90,0,0])
//         cylinder(r1=lm8uu_bearing_or+1, r2=lm8uu_bearing_or+0.1, h=2.1+width_spacer);
//
//         // Top left bearing hole taper
//         translate([lm8uu_bearing_walled_r,-0.05,lm8uu_bearing_upper_offset])
//         rotate([-90,0,0])
//         cylinder(r1=lm8uu_bearing_or+1, r2=lm8uu_bearing_or+0.1, h=2.1+width_spacer);
//
//         // Top right bearing hole taper
//         translate([0,width,0])
//         mirror([0,1,0])
//         translate([lm8uu_bearing_walled_r,-0.05,lm8uu_bearing_upper_offset])
//         rotate([-90,0,0])
//         cylinder(r1=lm8uu_bearing_or+1, r2=lm8uu_bearing_or+0.1, h=2.1+width_spacer);
//
//         // Bottom cutout for easier insertion of bearings
//         translate([lm8uu_bearing_walled_r+lm8uu_bearing_or-4,-1,lm8uu_bearing_lower_offset])
//         cube([10,width+2,lm8uu_bearing_cut]);
//
//         // Top cutout for easier insertion of bearings
//         translate([lm8uu_bearing_walled_r+lm8uu_bearing_or-4,-1,lm8uu_bearing_upper_offset])
//         cube([10,width+2,lm8uu_bearing_cut]);
//
//         // Mount holes
//         x_mount_holes();
//
//         // Belt cutouts
//         cutouts();
//     }
// }
//
// module bearing_bump(shift, smooth_radius) {
//     translate(shift)
//     rotate([-90,0,0])
//     minkowski() {
//         cylinder(r=lm8uu_bearing_walled_r-1, h=width-additional_width-2);
//         sphere(r=smooth_radius);
//     }
// }
//
// module bearing_spacer() {
//     difference() {
//         translate([0,(width-1.8)/2,0]) cube([bearing_depth, 1.8, height+5]);
//         translate([bearing_depth,(width-4)/2,lm8uu_bearing_lower_offset]) rotate([-90,0,0]) cylinder(r=(lm8uu_bearing_od/2)-0.6, h=4);
//         translate([bearing_depth,(width-4)/2,lm8uu_bearing_upper_offset]) rotate([-90,0,0]) cylinder(r=(lm8uu_bearing_od/2)-0.6, h=4);
//     }
// }
//
// module x_carriage_base() {
//     union() {
//         minkowski() {
//             translate([1,1,1])
//             cube([depth-2, width-2, height+12]);
//             sphere(r=1);
//         }
//         bearing_bump([lm8uu_bearing_walled_r,width_spacer+1,lm8uu_bearing_walled_r], bump_smooth_radius);
//         bearing_bump([lm8uu_bearing_walled_r,width_spacer+1,lm8uu_bearing_walled_r+rod_distance], bump_smooth_radius);
//     }
// }
//
// module x_carriage() {
//     union() {
//         difference() {
//             x_carriage_base();
//             x_carriage_holes();
//         }
//         bearing_spacer();
//     }
// }
//
// module cutouts() {
//     translate([1,0,35])
//     union() {
//         cube([13.5,12,20]);
//         translate([0,width-11.75,0])
//         cube([13.5,12,20]);
//     }
// }
//
//bearing(translation=[lm8uu_bearing_walled_r,2+width_spacer,lm8uu_bearing_lower_offset]);
//bearing(translation=[lm8uu_bearing_walled_r,width-lm8uu_bearing_ln-(2+width_spacer),lm8uu_bearing_lower_offset]);
//bearing(translation=[lm8uu_bearing_walled_r,2+width_spacer,lm8uu_bearing_lower_offset+rod_distance]);
//bearing(translation=[lm8uu_bearing_walled_r,width-lm8uu_bearing_ln-(2+width_spacer),lm8uu_bearing_lower_offset+rod_distance]);
//rod(translation=[lm8uu_bearing_walled_r,-250,lm8uu_bearing_lower_offset]);
//rod(translation=[lm8uu_bearing_walled_r,-250,lm8uu_bearing_lower_offset+rod_distance]);
//x_carriage();
