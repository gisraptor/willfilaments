// Vitamins for designing a 3d printer
$fn=64;

module rod(radius=4,length=500,rotation=[-90,0,0],translation=[0,0,0]) {
   translate(translation)
   rotate(rotation)
   color("Silver")
   cylinder(r=radius, h=length);
}

module linear_bearing(
	outer_radius=15/2,
	inner_radius=8/2,
	length=24,
	face_inset=0.1,
	ring_offset=4,
	rotation=[-90,0,0],
	translation=[0,0,0]) {
	translate(translation)
    rotate(rotation)
    difference() {
		// body
		color("Silver")
		cylinder(r=outer_radius, h=length);
		// holes, grooves, insets reside here
		color("DimGray")
		union() {
			// inner bearing hole
			translate([0,0,-1])
			cylinder(r=inner_radius, h=length+2);

			// left inset
			difference() {
				translate([0,0,-1])
				cylinder(r=outer_radius-0.2, h=1.2);
				translate([0,0,-1+face_inset])
				cylinder(r=inner_radius+0.2, h=1.2+face_inset);
			}

			// right inset
			translate([0,0,length+0.9])
			mirror() {
				difference() {
					translate([0,0,-1])
					cylinder(r=outer_radius-0.2, h=1.2);
					translate([0,0,-1+face_inset])
					cylinder(r=inner_radius+0.2, h=1.2+face_inset);
				}
			}

			// left outer ring
			translate([0,0,ring_offset])
			difference() {
				cylinder(r=outer_radius+1, h=0.5);
				translate([0,0,-0.5])
				cylinder(r=outer_radius-0.1, h=2);
			}

			// right outer ring
			translate([0,0,length-ring_offset])
			difference() {
				cylinder(r=outer_radius+1, h=0.5);
				translate([0,0,-0.5])
				cylinder(r=outer_radius-0.1, h=2);
			}
		}
    }
}

module ball_bearing(
    outer_diameter=22,
    inner_diameter=8,
    thickness=7,
	rotation=[0,0,0],
	translation=[20,20,0]) {
    
    outer_radius=outer_diameter/2;
    inner_radius=inner_diameter/2;
    translate(translation)
    rotate(rotation)
    union() {
        // outer rim
        difference() {
            cylinder(r=outer_radius, h=thickness);
            translate([0,0,-1]) cylinder(r=outer_radius-0.5, h=thickness+2);
        }
        // inner rim
        difference() {
            cylinder(r=inner_radius+0.5, h=thickness);
            translate([0,0,-0.5]) cylinder(r=inner_radius, h=thickness + 1);    
        }
        // bearing ring
        difference() {
            translate([0,0,0.2]) color("gray") cylinder(r=outer_radius-0.1, h=thickness-0.4);
            translate([0,0,-0.5]) cylinder(r=inner_radius+0.1, h=thickness + 1);
        }
    }
}

module hexagon(
    width=5.5,
    depth=2.4,
    sides=6,
	rotation=[0,0,0],
	translation=[0,0,0]) {
    
    side_ln = width*tan(30);
    width_across_corners = pow(pow(width, 2) + pow(side_ln, 2), 0.5);

    translate(translation)
    rotate(rotation)
    cylinder(r=width_across_corners/2, h=depth, $fn=sides);
}

linear_bearing();
rod();
ball_bearing();
nut(translation=[-10,-10,0]);