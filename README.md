# Will Filaments

This is my attempt to build a Wilson II 3d printer as designed
by Martin Rice (mjrice). The original designs can be found at
<https://github.com/mjrice/wilson2>. This design replaces the original
LM10UU linear bearings and 10mm rods with LM8UU bearings and 8mm rods. This
change has resulted in the need to update several files. These are the files
that we have created in order to accomplish this change. Not all files are
my own. In fact, most of the STL files were generated from OpenSCAD files
provided by others. Please see the ATTRIBUTIONS.md file for details and source
materials. I am very grateful to the wonderful RepRap community for putting
these designs together so my son and I can embark on this fun adventure!

Where not otherwise noted, this work is licensed under the Creative Commons
Attribution 4.0 International License. To view a copy of this license, visit
http://creativecommons.org/licenses/by/4.0/ or read the accompanying LICENSE
file.
